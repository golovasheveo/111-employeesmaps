package telran.employees.tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import telran.employees.api.EmployeesService;
import telran.employees.dto.Employee;
import telran.employees.dto.EmployeesReturnCodes;
import telran.employees.service.EmployeesServiceMapsImpl;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.stream;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class EmpTests {
    EmployeesService EmployeesService;

    @BeforeEach
    @Test
    void testAdd ( ) {
        EmployeesService = new EmployeesServiceMapsImpl ( );

        Employee[] data = {
                new Employee ( 100, 6000, "Company1", LocalDate.of ( 1983, 10, 29 ), "Name0" ),
                new Employee ( 101, 7000, "Company2", LocalDate.of ( 2001, 1, 11 ), "Name1" ),
                new Employee ( 102, 8000, "Company3", LocalDate.of ( 2002, 2, 12 ), "Name2" ),
                new Employee ( 103, 9000, "Company3", LocalDate.of ( 2003, 3, 13 ), "Name3" ),
                new Employee ( 104, 10000, "Company4", LocalDate.of ( 2004, 4, 14 ), "Name4" ),
                new Employee ( 105, 11000, "Company2", LocalDate.of ( 2005, 5, 15 ), "Name5" ),
                new Employee ( 106, 12000, "Company2", LocalDate.of ( 2006, 6, 16 ), "Name6" ),
                new Employee ( 107, 13000, "Company0", LocalDate.of ( 2007, 7, 17 ), "Name7" ),
                new Employee ( 108, 13000, "Company1", LocalDate.of ( 2008, 8, 18 ), "Name00" ),
                new Employee ( 109, 13000, "Company2", LocalDate.of ( 2009, 9, 19 ), "Name11" ),
                new Employee ( 110, 13000, "Company3", LocalDate.of ( 2010, 10, 20 ), "Name22" ),

        };

        stream ( data ).forEach ( employee -> assertEquals ( EmployeesReturnCodes.OK, EmployeesService.addEmployee ( employee ) ) );
    }

    @Test
    void testAddExist ( ) {
        Employee[] data = {
                new Employee ( 100, 6000, "Company1", LocalDate.of ( 1983, 10, 29 ), "Name0" ),
                new Employee ( 101, 7000, "Company2", LocalDate.of ( 2001, 1, 11 ), "Name1" ),
                new Employee ( 102, 8000, "Company3", LocalDate.of ( 2002, 2, 12 ), "Name2" ),
        };

        for ( Employee employee : data ) {
            assertEquals ( EmployeesReturnCodes.EMPLOYEE_ALREADY_EXISTS,
                           EmployeesService.addEmployee ( employee ) );
        }
    }

    @Test
    void testRemove ( ) {
        assertEquals ( EmployeesReturnCodes.OK, EmployeesService.removeEmployee ( 100 ) );
        assertNull ( EmployeesService.getEmployee ( 100 ) );
        assertEquals ( EmployeesReturnCodes.EMPLOYEE_NOT_FOUND, EmployeesService.removeEmployee ( 100 ) );

        Collection<Employee> test = (Collection<Employee>) EmployeesService.getEmployeesAges ( 30, 40 );
        assertEquals ( 0, test.size ( ) );

        Collection<Employee> testSize = (Collection<Employee>) EmployeesService.getEmployees ( );
        assertEquals ( 10, testSize.size ( ) );

        assertEquals ( EmployeesReturnCodes.OK, EmployeesService.removeEmployee ( 110 ) );
        assertNull ( EmployeesService.getEmployee ( 110 ) );
        assertEquals ( EmployeesReturnCodes.EMPLOYEE_NOT_FOUND, EmployeesService.removeEmployee ( 110 ) );

        Collection<Employee> testSizeC2 = (Collection<Employee>) EmployeesService.getEmployeesCompany ( "Company3" );
        assertEquals ( 2, testSizeC2.size ( ) );
    }

    @Test
    void testGet ( ) {
        Employee employee = EmployeesService.getEmployee ( 100 );
        assertEquals ( "Name0", employee.getName ( ) );
        assertEquals ( LocalDate.of ( 1983, 10, 29 ), employee.getBirthYear ( ) );

        Collection<Employee> test = (Collection<Employee>) EmployeesService.getEmployees ( );
        assertEquals ( 11, test.size ( ) );

    }


    @Test
    void testUpdateSalaryCompany ( ) {
        assertEquals ( EmployeesReturnCodes.EMPLOYEE_NOT_FOUND, EmployeesService.updateSalary ( 10000, 10000 ) );
        assertEquals ( EmployeesReturnCodes.OK, EmployeesService.updateSalary ( 100, 50000 ) );
        assertEquals ( 50000, EmployeesService.getEmployee ( 100 ).getSalary ( ) );

        assertEquals ( EmployeesReturnCodes.EMPLOYEE_NOT_FOUND, EmployeesService.updateCompany ( 10000, "Gemalto" ) );
        assertEquals ( EmployeesReturnCodes.OK, EmployeesService.updateCompany ( 100, "Gemalto" ) );
        assertEquals ( "Gemalto", EmployeesService.getEmployee ( 100 ).getCompany ( ) );
    }

    @Test
    void testGetSalary ( ) {
        Collection<Employee> result = (Collection<Employee>) EmployeesService.getEmployeesSalary ( 5000, 13000 );
        assertEquals ( 11, result.size ( ) );

        Collection<Employee> res = (Collection<Employee>) EmployeesService.getEmployeesSalary ( 13000, 13000 );
        assertEquals ( 4, res.size ( ) );
    }

    @Test
    void testGetCompany ( ) {
        Collection<Employee> test = (Collection<Employee>) EmployeesService.getEmployeesCompany ( "Company2" );
        assertEquals ( 4, test.size ( ) );
    }

    @Test
    void testGetAges ( ) {
        Collection<Employee> test = (Collection<Employee>) EmployeesService.getEmployeesAges ( 0, 35 );
        assertEquals ( 10, test.size ( ) );
    }

    @Test
    void testGroupSalary()
    {
        Map<String, List<Employee>> map = EmployeesService.getEmployeesGroupedBySalary ( 10000 );
        assertEquals ( 4, map.get ( "0-10000" ).size ( ) );
        assertEquals ( 7, map.get ( "10000-20000" ).size ( ) );
    }
}