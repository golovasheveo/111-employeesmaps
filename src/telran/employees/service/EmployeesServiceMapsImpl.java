package telran.employees.service;

import telran.employees.api.EmployeesService;
import telran.employees.dto.Employee;
import telran.employees.dto.EmployeesReturnCodes;
import telran.employees.dto.changeManager;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmployeesServiceMapsImpl implements EmployeesService {
    private final HashMap<Long, Employee>          employees        = new HashMap<> ( );
    //key-company, value-list of employees working for that company
    private final HashMap<String, List<Employee>>  employeesCompany = new HashMap<> ( );
    /**************************************************/
    //key - age, value - list of employees with that age
    private final TreeMap<Integer, List<Employee>> employeesAge     = new TreeMap<> ( );
    /*****************************************************/
    //key - salary, value - list of employees with that salary
    private final TreeMap<Integer, List<Employee>> employeesSalary  = new TreeMap<> ( );

    /******************************************************/


    @Override
    public EmployeesReturnCodes addEmployee ( Employee empl ) {
        Employee res = employees.putIfAbsent ( empl.getId ( ), empl );
        if ( res != null ) {
            return EmployeesReturnCodes.EMPLOYEE_ALREADY_EXISTS;
        }
        addEmployeeSalary ( empl );
        addEmployeeAge ( empl );
        addEmployeeCompany ( empl );
        return EmployeesReturnCodes.OK;

    }

    private void addEmployeeCompany ( Employee empl ) {
        String company = empl.getCompany ( );
        List<Employee> listEmployeesCompany =
                employeesCompany.getOrDefault ( company, new ArrayList<> ( ) );
        listEmployeesCompany.add ( empl );
        employeesCompany.putIfAbsent ( company, listEmployeesCompany );

    }

    private void addEmployeeAge ( Employee empl ) {
        Integer age = empl.getAge ( );
        List<Employee> listEmployeesCompany =
                employeesAge.getOrDefault ( age, new ArrayList<> ( ) );
        listEmployeesCompany.add ( empl );
        employeesAge.putIfAbsent ( age, listEmployeesCompany );
    }

    private void addEmployeeSalary ( Employee empl ) {
        Integer salary = empl.getSalary ( );
        List<Employee> listEmployeesCompany =
                employeesSalary.getOrDefault ( salary, new ArrayList<> ( ) );
        listEmployeesCompany.add ( empl );
        employeesSalary.putIfAbsent ( salary, listEmployeesCompany );
    }

    @Override
    public EmployeesReturnCodes removeEmployee ( long id ) {

        Employee employee = employees.get ( id );
        if ( employee == null ) return EmployeesReturnCodes.EMPLOYEE_NOT_FOUND;

        removeObject ( employee, employee.getCompany ( ), employeesCompany );
        removeObject ( employee, employee.getAge ( ), employeesAge );
        removeObject ( employee, employee.getSalary ( ), employeesSalary );

        employees.remove ( id );

        return EmployeesReturnCodes.OK;
    }

    private <T> void removeObject ( Employee employee, T key, Map<T, List<Employee>> map ) {
        map.get ( key ).remove ( employee );
    }


    @Override
    public Employee getEmployee ( long id ) {

        return employees.get ( id );
    }

    @Override
    public Iterable<Employee> getEmployees ( ) {

        return employees.values ( );
    }

    @Override
    public Iterable<Employee> getEmployeesCompany ( String company ) {

        return employeesCompany.getOrDefault ( company, new ArrayList<> ( ) );
    }

    @Override
    public Iterable<Employee> getEmployeesAges ( int ageFrom, int ageTo ) {

        return getFromSubMap ( employeesAge.subMap ( ageFrom, true, ageTo, true ) );

    }

    @Override
    public Iterable<Employee> getEmployeesSalary ( int salaryFrom, int salaryTo ) {
        return getFromSubMap ( employeesSalary.subMap ( salaryFrom, true, salaryTo, true ) );
    }

    private Iterable<Employee> getFromSubMap ( SortedMap<Integer, List<Employee>> subMap ) {

/*
        Set<Employee> result = new HashSet<> ( );
        subMap.values ( ).forEach ( result::addAll );
        return result;
*/

        Set<Employee> result = subMap
                .values ( )
                .stream ( )
                .flatMap ( Collection::stream )
                .collect ( Collectors.toSet ( ) );
        return result;
    }

    @Override
    public EmployeesReturnCodes updateCompany ( long id, String newCompany ) {

        Employee employee = employees.get ( id );
        if ( employee == null ) return EmployeesReturnCodes.EMPLOYEE_NOT_FOUND;
        removeEmployee ( id );
        addEmployee ( changeManager.updateCompany ( employee, newCompany ) );

        return EmployeesReturnCodes.OK;
    }

    @Override
    public EmployeesReturnCodes updateSalary ( long id, int newSalary ) {

        Employee employee = employees.get ( id );
        if ( employee == null ) return EmployeesReturnCodes.EMPLOYEE_NOT_FOUND;
        removeEmployee ( id );
        addEmployee ( changeManager.updateSalary ( employee, newSalary ) );

        return EmployeesReturnCodes.OK;
    }

    @Override
    public Map<String, List<Employee>> getEmployeesGroupedBySalary(int interval)
    {
            Map<String,
                    List<Employee>> res = employees.values ().stream ()
                                        .collect ( Collectors.groupingBy ( e -> {
                                            int minIntervalValue = e.getSalary () / interval*interval;
                                            return String.format ( "%d-%d", minIntervalValue, minIntervalValue+interval );

                                        } ) );
            return res;

    }
}
