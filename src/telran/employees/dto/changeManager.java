package telran.employees.dto;

public class changeManager {
    private static Employee replace ( Employee employee, String newCompany, int newSalary ) {
        return new Employee (
                employee.getId ( ),
                newSalary,
                newCompany,
                employee.getBirthYear ( ),
                employee.getName ( )
        );
    }

    public static Employee updateCompany ( Employee employee, String newCompany ) {
        return replace ( employee, newCompany, employee.getSalary ( ) );
    }

    public static Employee updateSalary ( Employee employee, int newSalary ) {
        return replace ( employee, employee.getCompany ( ), newSalary );
    }
}
